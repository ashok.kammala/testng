package cot.automation.testing.extended.tests;

import java.io.*;

import org.testng.annotations.Test;
public class Precondition4743 {  
	
	@Test(priority=11)
	    public void DeleteAfile() 
	    { 
	        File file = new File("command\\newtestdevice1 sim\\TestNewDevice4.properties"); 
	          
	        if(file.delete()) 
	        { 
	            System.out.println("File deleted successfully"); 
	        } 
	        else
	        { 
	            System.out.println("Failed to delete the file"); 
	        } 
	    } 
}
