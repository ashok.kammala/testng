package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity5526 {
	static String pass = "";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String uname2 = "";
	static String newpass="";

	@Test(priority=17)
	public void TestMethod5526() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);

			baseurl = prop.getProperty("url");
			uname = prop.getProperty("username");
			uname2 = prop.getProperty("username2");
			pass = prop.getProperty("password");
			newpass = prop.getProperty("newpassword");
			Dpath = prop.getProperty("DriverPathc");
			ph = prop.getProperty("ph1");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		Thread.sleep(3000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Devices"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_All_devices"))));
		element.click();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1_Device_Management"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking"))));
		element.click();
		
		ApiTest obj=new ApiTest();
		obj.manualMode();
		Thread.sleep(2000);
		obj.updatePosition();
		Thread.sleep(4000);
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		Thread.sleep(4000);
		
		((JavascriptExecutor) driver).executeScript("window.open()");

		Thread.sleep(2000);
		
		ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));
		
		driver.get(baseurl);
		Thread.sleep(5000);
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Groups_Dropdown"))));
		element.click();
		
		js.executeScript("window.scrollBy(0,1000)");
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Two_Devices_Group"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Widget"))));
		element.click();
		
		Thread.sleep(3000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Select_Widget"))));
		element.click();
		element.sendKeys("Map");
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Map_Widget"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Save_Button_Cockpit"))));
		element.click();
		Thread.sleep(4000);
		
		obj.sendCellInfo1();
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(0));
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		Thread.sleep(6000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		obj.sendCellInfo2();
		Thread.sleep(6000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		
		obj.sendMobile1();
		Thread.sleep(6000);

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		
		obj.sendMobile2();
		Thread.sleep(6000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		
		obj.sendWiFi1();
		Thread.sleep(6000);

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		
		obj.sendWiFi2();
		Thread.sleep(6000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tracking_Reload"))));
		element.click();
		
	}
}