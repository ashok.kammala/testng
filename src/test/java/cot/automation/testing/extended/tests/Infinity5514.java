package cot.automation.testing.extended.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


import java.util.List;

public class Infinity5514 {

	/** Check Help and Services
	 * @param args
	 * @throws InterruptedException
	 */
	@Test(priority=10)
	public static void infinity5514() throws InterruptedException, IOException{

		Properties prop = new Properties();
		FileInputStream input = null;
		String Dpath = null;
		String Driverpath = null;
		String baseurl = null;
		String username6 = null;
		String pass3 = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);
			baseurl = prop.getProperty("url");
			Dpath = prop.getProperty("DriverPathc");
			Driverpath = prop.getProperty("Driverpath");
			username6 = prop.getProperty("username6");
			pass3 = prop.getProperty("pass3");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		System.out.println(driver.getTitle());
		WebDriverWait wait = new WebDriverWait(driver, 2000);

		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))).sendKeys(username6);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Password"))).sendKeys(pass3);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Login"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Login"))));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_HelpService"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_HelpService"))).click();
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@class='locale-select is-size-18']")));
		driver.findElement(By.xpath("//select[@class='locale-select is-size-18']")).sendKeys("EN");
		Thread.sleep(5000);
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"\t"); 
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		List<WebElement> myElements = driver.findElements(By.xpath("//div[@class='basic-cards-list-item']//a"));
		System.out.println("elemt size" +myElements.size());		
		int testVar = 1;
		for(int i = 0; i <= 7; i++) {
			System.out.println("i value"+i);
			Thread.sleep(10000);		
			wait.until(ExpectedConditions.visibilityOf(myElements.get(i)));
			System.out.println(myElements.get(i).getText());
			if(!myElements.get(i).getText().contains("Cloud of Things - developer tools (SDKs & Agents)")) {
				myElements.get(i).click();
			}			
			Thread.sleep(15000);
			System.out.println("tabs size" + driver.getWindowHandles().size());
			testVar++;
			if(driver.getWindowHandles().size() == testVar) {
				driver.switchTo().window(tabs.get(0));
			} else {
			
		       testVar--;		       
			}
		}
	}
}

