package cot.automation.testing.extended.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Infinity3985 {

	@Test(priority=23)
	public static void infinity3985() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;
		String Dpath = null;
		String Driverpath = null;
		String baseurl = null;
		String username7 = null;
		String password = null;
		String Xpath_Username_Login_Page = null;
		String Xpath_Administration1 = null;
		String Xpath_Login = null;
		String Xpath_Password = null;
		String Xpath_Userbutton = null;
		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");
			prop.load(input);
			baseurl = prop.getProperty("urlManage");
			Dpath = prop.getProperty("DriverPathc");
			Driverpath = prop.getProperty("Driverpath");
			String ph = prop.getProperty("ph1");
			username7 = prop.getProperty("username7");
			password = prop.getProperty("password");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		System.out.println(driver.getTitle());
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		WebElement element;
		driver.manage().window().maximize();
		System.out.println("Successfully opened the website of CoT");
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))).sendKeys(username7);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Password"))).sendKeys(password);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Login"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Login"))));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //span[contains(text(),'Tenants')]")));
		driver.findElement(By.xpath("//span[contains(text(),'Tenants')]")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Usage statistics')]")));
		driver.findElement(By.xpath("//span[contains(text(),'Usage statistics')]")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Date from']")));
		element = driver.findElement(By.xpath("//input[@placeholder='Date from']"));
		element.click();
		element.clear();
		element.sendKeys("2017-08-01");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Date to']")));
		element = driver.findElement(By.xpath("//input[@placeholder='Date to']"));
		element.click();
		element.clear();
		element.sendKeys("2017-08-31");
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='btn btn-sm btn-success pull-right uib-close ng-binding']")));
		driver.findElement(By.xpath("//button[@class='btn btn-sm btn-success pull-right uib-close ng-binding']")).click();
		Thread.sleep(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='btn btn-link ng-binding ng-scope ng-isolate-scope']")));
		driver.findElement(By.xpath("//a[@class='btn btn-link ng-binding ng-scope ng-isolate-scope']")).click();
		Thread.sleep(5000);
		
		Actions hover = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_Download_Button_Statistics")));
		hover.moveToElement(element).build().perform();
		element.click();
		
		System.out.println("Downloaded");
		Thread.sleep(15000);
		System.out.println("Val1> " + getLatestFilefromDir("C:\\Users\\A94832877\\Downloads").toString());
		System.out.println("Val2> " + getSecondFilefromDir("C:\\Users\\A94832877\\Downloads").toString());

		File file1 = getLatestFilefromDir("C:\\Users\\A94832877\\Downloads");
		File file2 = getSecondFilefromDir("C:\\Users\\A94832877\\Downloads");

		String fileOneContent = FileUtils.readFileToString(file1, "UTF-8");
		String fileTwoContent = FileUtils.readFileToString(file2, "UTF-8");
		boolean comparision = StringUtils.equals(fileOneContent, fileTwoContent);
		if (comparision) {
			System.out.println("File contents are same!");
		} else {
			System.out.println("File contents are not same!");
		}
	}
	private static File getLatestFilefromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}
		File lastModifiedFile = files[0];
		File secondModified = null;
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;
	}
	private static File getSecondFilefromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}

		File lastModifiedFile = files[0];
		File secondModified = null;
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				secondModified = lastModifiedFile;
				lastModifiedFile = files[i];
			}
		}
		return secondModified;
	}
	/**
	 * gets the MD5Hash of the input string
	 * 
	 * @param input
	 * @return
	 */
	public static String getMd5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger no = new BigInteger(1, messageDigest);
			String hashtext = no.toString(16);
			while (hashtext.length() < 32) {
				hashtext = "0" + hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
	}

}

