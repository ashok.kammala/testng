package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Infinity5527 {
	static String pass="";
	static String newpass="";
	static String baseurl="";
	static String uname="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	
	
	@Test(priority=21)
	public void testMethod5527() throws InterruptedException, IOException {
		
		 Properties prop = new Properties();
		    InputStream input = null;

		    try {

		        input = new FileInputStream("src/main/resources/configuarations.properties");

		        prop.load(input);

		        baseurl=prop.getProperty("url");
		        uname=prop.getProperty("username");
		        uname2=prop.getProperty("username2");
		        pass=prop.getProperty("password");
		        newpass=prop.getProperty("newpassword");
		        Dpath=prop.getProperty("DriverPathc");
		        ph=prop.getProperty("ph1");
		        
		        
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    } finally {
		        if (input != null) {
		            try {
		                input.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		    
		    
	System.setProperty("webdriver.chrome.driver", Dpath);
 	WebDriver driver = new ChromeDriver();
 	 driver.get(baseurl);
 	 driver.manage().window().maximize();
 	 
 	WebDriverWait wait=new WebDriverWait(driver, 120);
 	WebElement element;
 	
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
	element.sendKeys(uname);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
	element.sendKeys(newpass);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
	element.click();
		Thread.sleep(5000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Groups_Dropdown_5527"))));
		element.click();
		Thread.sleep(2000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Cot_Test_group"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_PlusIcon"))));
		element.click();
		Thread.sleep(5000);
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_PlusIcon_NewDashboard"))));
		element.click();
		element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_NewDashboard_Create_Table"))));
		element.clear();
		element.sendKeys("TestDashboard");
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Create_Table_Save"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_click"))));
	element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_AddWidget_Button"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_CreateWidget_Cancel_Button"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_NewDashboard_Edit"))));
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Edit"))));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Edit_Cancel")));
		int y = element.getLocation().getY();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0," + y + ")");
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Edit_Cancel"))).click();
		Thread.sleep(2000);
		
		element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_NewDashboard_AddWidget_Button"))));
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_AddWidget_Button"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Select_Widget"))));
		element.click();
		List<WebElement> myElements = driver
				.findElements(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Dropdown")));
		for (WebElement e : myElements) {
			if (e.getText().equalsIgnoreCase("Magenta Asset Table")) {
				e.click();
				break;
			}
		}
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Save_Button")));
		int y2 = element.getLocation().getY();
		JavascriptExecutor js2 = (JavascriptExecutor) driver;
		js2.executeScript("window.scrollTo(0," + y2 + ")");
		Thread.sleep(3000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Save_Button"))));
		element.click();
		Thread.sleep(5000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Lock/Unlock_Butoon"))));
		element.click();
		Thread.sleep(3000);
		if (driver.findElement(By.xpath(
				"//div[@id='page-toolbar']//ul[@class='nav navbar-nav navbar-right']//li[@class='navbar-form ng-scope']//button[@title='Lock/unlock this dashboard']//i[@class='fa fw fa-lock']")) != null) {
			try {
				driver.findElement(By.xpath("//a[@class='btnIcon dropdown-toggle c8y-dropdown']"));
			} catch (NoSuchElementException e) {
				System.out.println("widget is not editable " + e.getMessage());
			}
		}
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Lock/Unlock_Butoon"))));
		element.click();

		if (driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Lock_Edit"))) != null) {
			element = wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Toggle_Button"))));
			element.click();
			element = wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Toggle_EditButton"))));
			element.click();
			element = driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_CreateWidget_Cancel_Button")));
			int y3 = element.getLocation().getY();
			JavascriptExecutor js3 = (JavascriptExecutor) driver;
			js3.executeScript("window.scrollTo(0," + y3 + ")");
			element = wait.until(ExpectedConditions
					.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_CreateWidget_Cancel_Button"))));
			element.click();
			WebElement From = driver
					.findElement(By.xpath("//div[@class='card-header-actions draggableCursor drag-handle']"));
			Actions act = new Actions(driver);
			act.dragAndDropBy(From, 400, 80).build().perform();
			Thread.sleep(2000);
			WebElement menuOption = driver
					.findElement(By.xpath("//div[@class='card card-dashboard grid-stack-item-content ng-scope']"));
			act.moveToElement(menuOption).perform();
			Thread.sleep(2000);
			WebElement resizeable = driver.findElement(By.xpath(
					"//div[@class='ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se']"));
			Action resize = act.clickAndHold(resizeable).moveByOffset(300, 150).release().build();
			resize.perform();
		}
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_More_Butotn"))));
		element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_RemoveDashboard_Button"))));
		element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_ModalWindow_OK_Button"))));
		element.click();
		Thread.sleep(2000);
		driver.close();

	}
}
