package cot.automation.testing.extended.tests;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Precondition4747 {
	static String DeviceID="620134";

	static String pass="";
	static String pass1="";
	static String baseurl1="";
	static String baseurl="";
	static String uname1="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	static String uname3="";
	static String baseurl2="";
	static String newpass="";
	
	@Test(priority=19)
	public void PreTestMethod4747() throws InterruptedException, IOException {
		
		 Properties prop = new Properties();
		    InputStream input = null;

		    try {

		        input = new FileInputStream("src/main/resources/configuarations.properties");

		        prop.load(input);

		        baseurl=prop.getProperty("url");
		        baseurl1=prop.getProperty("RamURL1");
		        uname1=prop.getProperty("RamUser1");
		        uname2=prop.getProperty("username2");
		        uname3=prop.getProperty("username");
		        pass1=prop.getProperty("RamPass1");
		        Dpath=prop.getProperty("DriverPathc");
		        ph=prop.getProperty("ph1");
		        pass=prop.getProperty("password");
		        newpass=prop.getProperty("newpassword");
		        baseurl2=prop.getProperty("urlGmail");
		        
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    } finally {
		        if (input != null) {
		            try {
		                input.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		       
		System.setProperty("webdriver.chrome.driver", Dpath);
	 	WebDriver driver = new ChromeDriver();
	 	 driver.get(baseurl);
	 	 driver.manage().window().maximize();
	 	 
	 	WebDriverWait wait=new WebDriverWait(driver, 180);
	 	WebElement element;
	 	
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname3);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		Thread.sleep(3000);
		
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		Thread.sleep(3000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Devices"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_All_devices"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1_Device_Management"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarms_NewDevice1"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Clear_All"))));
		element.click();
		Thread.sleep(8000);
		
		Actions hover = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_Clear_All_OK")));
		hover.moveToElement(element).build().perform();
		element.click();
		
		Thread.sleep(4000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Clear_All"))));
		element.click();
		Thread.sleep(8000);
		
		Actions hover2 = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_Clear_All_OK")));
		hover2.moveToElement(element).build().perform();
		element.click();
		
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Reload_Alarms"))));
		element.click();
		Thread.sleep(3000);
	}
}
