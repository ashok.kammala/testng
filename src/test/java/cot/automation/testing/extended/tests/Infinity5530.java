package cot.automation.testing.extended.tests;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.openqa.selenium.OutputType;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.firefox.FirefoxDriver;

public class Infinity5530 {
	static String pass="";
	static String newpass="";
	static String baseurl="";
	static String uname="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	
	
	@Test(priority=8)
	public void infinity5530() throws InterruptedException, IOException  {
		
		Properties prop = new Properties();
		InputStream input = null;
		String Dpath = null;
		String Driverpath = null;
		String username7 = null;
		String password = null;
		String Xpath_Username_Login_Page = null;
		String Xpath_Administration1 = null;
		String Xpath_Login = null;
		String Xpath_Password = null;
		String Xpath_Userbutton = null;
		
		input = new FileInputStream("src/main/resources/configuarations.properties");
		prop.load(input);
		Dpath = prop.getProperty("DriverPathc");
		username7 = prop.getProperty("username7");
		password = prop.getProperty("password");
		String baseurl3 = prop.getProperty("baseurl3");
		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl3);
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		WebElement element;
		driver.manage().window().maximize();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))).sendKeys("ankituser4");
		driver.findElement(By.xpath(prop.getProperty("Xpath_Password"))).sendKeys("-Lucky1234AmiT");	
	    driver.findElement(By.xpath(prop.getProperty("Xpath_Login"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Userbutton"))));
	    driver.findElement(By.xpath(prop.getProperty("Xpath_Userbutton"))).click();
	    if(driver.findElements(By.xpath("//div[@id='app']//div[2]//div[5]")).size() != 0){
	    	System.out.println("Element is Present");
	    	}
	    else{
	    	System.out.println("Only Associated User Heirarchy can be seen");
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
	    driver.findElement(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
	    driver.findElement(By.xpath(prop.getProperty("Xpath_Device_management"))).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Devices')]")));
	    driver.findElement(By.xpath("//span[contains(text(),'Devices')]")).click();
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'All devices')]")));
	    driver.findElement(By.xpath("//span[contains(text(),'All devices')]")).click();
	    if(driver.findElements(By.xpath("//tr[32]//td[2]//div[1]//a[1]")).size() != 0){
	    	System.out.println("Element is Present");
	    	}
	    else{
	    	System.out.println("Only Associated Devices Heirarchy can be seen");
	    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[1]//td[2]//div[1]//a[1]")));
	 	driver.findElement(By.xpath("//tr[1]//td[2]//div[1]//a[1]")).click();	
	 	if(driver.findElements(By.xpath("//translate[contains(text(),'Edit')]")).size() != 0){
	    	System.out.println("Element is Present");
	    	}
	    else{
	    	
	    	System.out.println("This User cannot Edit the Device Name");
//	    TODO check the elements present or not 
	    }
	    }
	}
}

}
