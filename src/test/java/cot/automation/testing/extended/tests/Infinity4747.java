package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity4747 {

static String DeviceID="620134";

	static String pass="";
	static String pass1="";
	static String baseurl1="";
	static String baseurl="";
	static String uname1="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	static String uname3="";
	static String baseurl2="";
	static String newpass="";
	
	@Test(priority=20)
	public void TestMethod4747() throws InterruptedException, IOException {
		
		 Properties prop = new Properties();
		    InputStream input = null;

		    try {

		        input = new FileInputStream("src/main/resources/configuarations.properties");

		        prop.load(input);

		        baseurl=prop.getProperty("url");
		        baseurl1=prop.getProperty("RamURL1");
		        uname1=prop.getProperty("RamUser1");
		        uname2=prop.getProperty("username2");
		        uname3=prop.getProperty("username");
		        pass1=prop.getProperty("RamPass1");
		        Dpath=prop.getProperty("DriverPathc");
		        ph=prop.getProperty("ph1");
		        pass=prop.getProperty("password");
		        newpass=prop.getProperty("newpassword");
		        baseurl2=prop.getProperty("urlGmail");
		        
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    } finally {
		        if (input != null) {
		            try {
		                input.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		       
		    String new_dir="command\\indiatransition sim";
		    Runtime rt = Runtime.getRuntime();
		    Process process =rt.exec("cmd.exe /c cd \""+new_dir+"\" & start cmd.exe /k \"java -jar JavaClient.jar\"");
		    
		    
		System.setProperty("webdriver.chrome.driver", Dpath);
	 	WebDriver driver = new ChromeDriver();
	 	 driver.get(baseurl);
	 	 driver.manage().window().maximize();
	 	 
	 	WebDriverWait wait=new WebDriverWait(driver, 180);
	 	WebElement element;
	 	
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname3);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		Thread.sleep(2000);
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Configuration_Cockpit"))));
		element.click();
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Global_Smart_Rules"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
		element.click();
		
		
		String Type1="TestAlarm1";
		Thread.sleep(2000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Send_Email"))));
		element.click();
		Thread.sleep(2000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarm_Type"))));
		element.click();
		element.clear();
		element.sendKeys(Type1);
		
		Thread.sleep(2000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,100)");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Send_to"))));
		element.sendKeys(uname2);
		
		js.executeScript("window.scrollBy(0,1000)");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
		element.sendKeys(DeviceID);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
		element.click();
		
		Thread.sleep(3000);
		ApiTest obj=new ApiTest();
		obj.setAlarm(Type1);
		
		((JavascriptExecutor) driver).executeScript("window.open()");

		Thread.sleep(2000);
		
		ArrayList<String> tabs1 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

	    driver.get(baseurl);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		WebElement element2;
		Thread.sleep(4000);
	    element2= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Overviews"))));
		element2.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarms"))));
		element.click();
	
		Thread.sleep(30000);
		
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(2));
		
	    driver.get(baseurl2);
	    
	    element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login_email"))));
		element.sendKeys(uname2);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login_Next_Button"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login_Password"))));
		element.sendKeys(pass);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password_Next_Button"))));
		element.click();
		 
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Select_Alarm_Email"))));
		element.click();
		
		try
	    {	
			element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Show_Quoted_Text"))));
	        if(element.isDisplayed())	
	        {
	            element.click();
	            Thread.sleep(4000);
	        }
	        else{
	        	Thread.sleep(4000);
	        	
	        }
	    }
		catch(Exception e){
			
		}
		
		 
		ArrayList<String> tabs3 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs3.get(0));
		driver.navigate().refresh();						
		
		String Type2="TestAlarm2";
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
		element.click();
		Thread.sleep(4000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Send_SMS"))));
		element.click();
		Thread.sleep(2000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarm_Type"))));
		element.click();
		element.clear();
		element.sendKeys(Type2);
		
		js.executeScript("window.scrollBy(0,1000)");		
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_phone_number_textbox"))));
		element.sendKeys(ph);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
		element.sendKeys(DeviceID);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
		element.click();
		
		js.executeScript("window.scrollBy(0,1000)");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
		element.click();
		
		Thread.sleep(3000);
		obj.setAlarm(Type2);
		
		String Type3="TestAlarm3";
		
		ArrayList<String> tabs4 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs4.get(0));
		driver.navigate().refresh();
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
		element.click();		
		Thread.sleep(4000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Increase_Severity"))));
		element.click();
		Thread.sleep(2000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarm_Type"))));
		element.click();
		element.clear();					
		element.sendKeys(Type3);
				
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Time_Interval"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Time_Unit_Minutes"))));
		element.click();
		
		js.executeScript("window.scrollBy(0,1000)");
				
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
		element.sendKeys(DeviceID);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
		element.click();
		
		js.executeScript("window.scrollBy(0,1000)");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
		element.click();	
		
		obj.setSeverityAlarm(Type3);
		ArrayList<String> tabs5 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs5.get(1));
		driver.navigate().refresh();
		
		Thread.sleep(100000);
		
		
				String Type4="TestAlarm4";
				ArrayList<String> tabs6 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs6.get(0));
				Thread.sleep(3000);
				driver.navigate().refresh();
				
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
				element.click();
				Thread.sleep(4000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Execute_Operation"))));
				element.click();
				Thread.sleep(2000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarm_Type1"))));
				element.click();
				element.clear();
				element.sendKeys(Type4);
						
				js.executeScript("window.scrollBy(0,1000)");
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Operation_Example"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Update_Relay"))));
				element.click();
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
				element.sendKeys(DeviceID);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
				element.click();

				js.executeScript("window.scrollBy(0,1000)");
				
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
				element.click();
				
				obj.setOperation();
				
				ArrayList<String> tabs7 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs7.get(2));
				driver.navigate().refresh();
				
				driver.get(baseurl);
				 
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Overviews"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_Control"))));
				element.click();
				driver.navigate().refresh();
				Thread.sleep(5000);
				
				
				ArrayList<String> tabs8 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs8.get(0));
				Thread.sleep(3000);
				driver.navigate().refresh();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
				element.click();
				Thread.sleep(4000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_On_Missing_Measurements"))));
				element.click();
				Thread.sleep(2000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarm_Type1"))));
				element.click();
				element.clear();
				element.sendKeys("Temperatur-A");
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Time_Interval"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Time_Unit_Minutes"))));
				element.click();
				
				js.executeScript("window.scrollBy(0,1000)");
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
				element.sendKeys(DeviceID);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
				element.click();

				js.executeScript("window.scrollBy(0,1000)");
				
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
				element.click();
				
				process.destroy();//stopping cmd prompt
				ArrayList<String> tabs13 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs13.get(1));
				driver.navigate().refresh();
				Thread.sleep(60000);
				driver.navigate().refresh();
				
		
				ArrayList<String> tabs9 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs9.get(0));
				Thread.sleep(3000);
				driver.navigate().refresh();
				process =rt.exec("cmd.exe /c cd \""+new_dir+"\" & start cmd.exe /k \"java -jar JavaClient.jar\"");
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
				element.click();
				Thread.sleep(4000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Explicit_Threshold"))));
				element.click();
				Thread.sleep(2000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Data_Point"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_TemperatureA"))));
				element.click();	
				js.executeScript("window.scrollBy(0,1000)");
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
				element.sendKeys(DeviceID);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
				element.click();

				js.executeScript("window.scrollBy(0,1000)");
				
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
				element.click();
				
				ArrayList<String> tabs11 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs11.get(1));
				driver.navigate().refresh();
	
				ArrayList<String> tabs10 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs10.get(0));
				driver.navigate().refresh();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Add_Smartrule"))));
				element.click();
				Thread.sleep(4000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Threshold"))));
				element.click();
				Thread.sleep(2000);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Data_Point"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_TemperatureA"))));
				element.click();	
				js.executeScript("window.scrollBy(0,1000)");
						
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_Device"))));
				element.sendKeys(DeviceID);
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_button_smartrule"))));
				element.click();

				js.executeScript("window.scrollBy(0,1000)");
				
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1"))));
				element.click();
				element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Create_Button"))));
				element.click();
				
				ArrayList<String> tabs12 = new ArrayList<String> (driver.getWindowHandles());
				driver.switchTo().window(tabs12.get(1));
				driver.navigate().refresh();
	}
}

