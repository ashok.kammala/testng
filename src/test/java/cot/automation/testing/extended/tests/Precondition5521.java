package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Precondition5521 {

	static String pass = "";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String urlmail = "";
	static String uname2 = "";
	static String uname3 = "";
	static String uname4 = "";
	static String uname5 = "";
	static String newalias = "Akshay.M.Navada1";
	static String passName = "AkshayMNavada1";

	@Test(priority=6)
	public void Test5521Method() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;
		String Dpath = null;
		String baseurl = null;
		String username6 = null;
		String pass3 = null;
	
		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);
			baseurl = prop.getProperty("url");
			Dpath = prop.getProperty("DriverPathc");
			username6 = prop.getProperty("username6");
			pass3 = prop.getProperty("pass3");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		System.out.println(driver.getTitle());
		String myUrl = null;
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))).sendKeys(username6);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Password"))).sendKeys(pass3);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Login"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Login"))));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Administration1"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Administration1"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Userbutton"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Userbutton"))).click();
		wait.until(ExpectedConditions		
				.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Three_Dots_Users"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Three_Dots_Users"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath(prop.getProperty("Xpath_Dots_Delete"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Dots_Delete")))			
				.click();
		Thread.sleep(5000);
		
		WebElement element;
		Actions hover = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_OK_Delete")));
		hover.moveToElement(element).build().perform();
		element.click();
		
		Thread.sleep(3000);

	}

}
