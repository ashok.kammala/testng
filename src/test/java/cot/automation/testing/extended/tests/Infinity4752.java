package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity4752 {

	static String DeviceID = "190859";

	static String pass = "";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String uname2 = "";
	static String newpass="";
	
	@Test(priority=14)
	public void TestMethod4752() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);

			baseurl = prop.getProperty("url");
			uname = prop.getProperty("username");
			uname2 = prop.getProperty("username2");
			pass = prop.getProperty("password");
			Dpath = prop.getProperty("DriverPathc");
			ph = prop.getProperty("ph1");
			newpass = prop.getProperty("newpassword");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;

		String new_dir = "command\\indiatransition sim";
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("cmd.exe /c cd \"" + new_dir + "\" & start cmd.exe /k \"java -jar JavaClient.jar\"");

		ApiTest obj = new ApiTest();

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();

		Thread.sleep(5000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		Thread.sleep(3000);
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Devices"))));
		element.click();
		Thread.sleep(3000);
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_All_devices"))));
		element.click();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,100)");
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1_Device_Management"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Alarms_NewDevice1"))));
		element.click();

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Realtime"))));
		element.click();

		obj.setAlarmTest4752();

		Thread.sleep(5000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Realtime"))));
		element.click();

		obj.setAlarmTest4752();

		process.destroy();
	}
}
