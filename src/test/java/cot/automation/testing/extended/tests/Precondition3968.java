package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Precondition3968 extends getMessages {

	static String final_ncode="";
	static String pass = "";
	static String baseurl2 = "";
	static String uname2 = "";
	static String Dpath = "";
	static String ph = "";
	static String urlmail = "";

	@Test(priority=3)
	public void Infinity3968method() throws InterruptedException, IOException {

		String vcode = "";
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);

			baseurl2 = prop.getProperty("urlManage");
			uname2 = prop.getProperty("username2");
			pass = prop.getProperty("password");
			Dpath = prop.getProperty("DriverPathc");
			ph = prop.getProperty("ph1");
			urlmail = prop.getProperty("urlGmail");
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl2);
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys("Akshay.M1");
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(pass);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();

		Thread.sleep(10000);

		((JavascriptExecutor) driver).executeScript("window.open()");

		Thread.sleep(30000);

		ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs1.get(1));

		driver.get("https://control.textlocal.in/messages/?id=1018192");

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_field_login"))));
		element.sendKeys(uname2);
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password_field_login"))));
		element.sendKeys(pass);
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_login_button_textlocal"))));
		element.click();
		driver.navigate().refresh();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Select_message"))));

		try {
			vcode = element.getText();

		} catch (StaleElementReferenceException e) {

		}
		System.out.println(vcode);
		String ncode = getVCode(vcode);
		System.out.println(ncode);
		
		if(ncode.length()==16) {
			String new_ncode = ncode.substring(0, ncode.length() - 6);
			final_ncode = new_ncode.substring(5);
			System.out.println(final_ncode);
		}
		
		else {
			String new_ncode = ncode.substring(0, ncode.length() - 12);
			final_ncode = new_ncode.substring(5);
			System.out.println(final_ncode);
		}

		Thread.sleep(5000);

		driver.switchTo().window(tabs1.get(0));

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_sms_token_textbox"))));
		element.sendKeys(final_ncode);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_sms_token_send_button"))));
		element.click();
		Thread.sleep(5000);

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Tenant_button"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Subtenant_button"))));
		element.click();
		Thread.sleep(20000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_cotregtest1"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Application_button"))));
		element.click();
		
		Thread.sleep(5000);
		Actions hover = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_Unsubscribe")));
		hover.moveToElement(element).build().perform();
		element.click();
		
		Thread.sleep(4000);

	}

	public static String getVCode(String s) {

		String numberOnly = s.replaceAll("[^0-9]", "");

		return numberOnly;
	}

	public static String getSMS(String s) {
		String smstrue = "";
		if (s.length() < 15) {
			smstrue = s.substring(s.length() - 15);
			if (smstrue.length() > 6) {
				smstrue = smstrue.substring(0, 6);
			}
		}
		return smstrue;
	}

}
