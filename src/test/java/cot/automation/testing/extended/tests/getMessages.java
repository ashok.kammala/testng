package cot.automation.testing.extended.tests;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
 
public class getMessages {

	String token="";
	public String getMessages1() throws IOException{
		try {
			// Construct data
			System.setProperty("java.net.useSystemProxies", "true");
			
			String apiKey = "apikey=" + URLEncoder.encode("X11Vo7eZwtw-2SyoQFXAD8K7lyWO6EmU0YNLDvsx8R", "UTF-8");
			String inbox_id = "&inbox_id=" + URLEncoder.encode("986540", "UTF-8");
			
			// Send data
			//Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.24.0.53", 8080));
			String data = "https://api.textlocal.in/get_messages/?" + apiKey + inbox_id;
			URL url = new URL(data);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(data);
			wr.flush();
			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			String sResult="";
			while ((line = rd.readLine()) != null) {
				// Process line...
				sResult=sResult+line+" ";
			}
			rd.close();
			
			return sResult;
		} catch (Exception e) {
			System.out.println("Error SMS "+e);
			return "Error "+e;
		}
	}
	
	public void setCode(String s) {
		this.token=s;
		

	}
	public String getCode() {
		
		String t=this.token;
		
		return t;
	}
	
	public static void main(String[] args) throws InterruptedException, IOException {
		
		//webdriver
		
		getMessages obj=new getMessages();
		String token1= obj.getMessages1();
		obj.setCode(token1);
		System.out.println(token1);
		
	}
	
	
	
}
