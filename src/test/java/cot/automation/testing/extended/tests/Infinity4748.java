package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity4748 {

	static String DeviceID = "620134";

	static String pass = "";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String uname2 = "";
	static String newpass="";
	
	@Test(priority=13)
	public void TestMethod4748() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);

			baseurl = prop.getProperty("url");
			uname = prop.getProperty("username");
			uname2 = prop.getProperty("username2");
			pass = prop.getProperty("password");
			newpass = prop.getProperty("newpassword");
			Dpath = prop.getProperty("DriverPathc");
			ph = prop.getProperty("ph1");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String new_dir = "command\\indiatransition sim";
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec("cmd.exe /c cd \"" + new_dir + "\" & start cmd.exe /k \"java -jar JavaClient.jar\"");

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		Thread.sleep(4000);
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Devices"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_All_devices"))));
		element.click();
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDevice1_Device_Management"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Measurements"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Realtime"))));
		element.click();
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Dropdown_Menu1"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Minutely"))));
		element.click();
		Thread.sleep(3000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Dropdown_Menu2"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Last_Hour"))));
		element.click();
		Thread.sleep(3000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Dropdown_Menu2"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Custom"))));
		element.click();
		Thread.sleep(3000);

		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Dropdown_Menu2"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Last_Hour"))));
		element.click();
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Date_Picker_Options1"))));
		element.click();
		element.clear();
		element.sendKeys("2019-08-26");

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Date_Picker_Options2"))));
		element.click();
		element.clear();
		element.sendKeys("2019-08-01");

		Thread.sleep(5000);

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Date_Picker_Options1"))));
		element.click();
		element.clear();
		element.sendKeys("2020-08-26");

		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Date_Picker_Options2"))));
		element.click();
		element.clear();
		element.sendKeys("2020-09-01");

		Thread.sleep(5000);

		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Dropdown_Menu2"))));
		element.click();
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Last_Minute"))));
		element.click();

		Thread.sleep(5000);

		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Realtime"))));
		element.click();
		Thread.sleep(5000);
		process.destroy();
	}
}
