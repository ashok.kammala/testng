package cot.automation.testing.extended.tests;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Precondition5523  {
	static String pass="";
	static String newpass="";
	static String baseurl="";
	static String uname="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	
	
	@Test(priority=15)
	public void testMethod5523() throws InterruptedException, IOException {
		
		 Properties prop = new Properties();
		    InputStream input = null;

		    try {

		        input = new FileInputStream("src/main/resources/configuarations.properties");

		        prop.load(input);

		        baseurl=prop.getProperty("url");
		        uname=prop.getProperty("username");
		        uname2=prop.getProperty("username2");
		        pass=prop.getProperty("password");
		        newpass=prop.getProperty("newpassword");
		        Dpath=prop.getProperty("DriverPathc");
		        ph=prop.getProperty("ph1");
		        
		        
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    } finally {
		        if (input != null) {
		            try {
		                input.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		    
		    
	System.setProperty("webdriver.chrome.driver", Dpath);
 	WebDriver driver = new ChromeDriver();
 	 driver.get(baseurl);
 	 driver.manage().window().maximize();
 	 
 	WebDriverWait wait=new WebDriverWait(driver, 120);
 	WebElement element;
 	
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
	element.sendKeys(uname);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
	element.sendKeys(newpass);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
	element.click();
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		Thread.sleep(5000);
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Groups"))));
		element.click();
		
		Thread.sleep(4000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_AAAGroup"))));
		if(element.isDisplayed()) {
			
			element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Delete_Group_Dots"))));
			element.click();
			element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Delete_Button1"))));
			element.click();
			element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Delete_The_Group"))));
			element.click();
			
		}
		else {
			Thread.sleep(3000);
		}

	}

}
