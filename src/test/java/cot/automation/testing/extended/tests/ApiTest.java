package cot.automation.testing.extended.tests;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.hamcrest.Matchers;

import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

public class ApiTest {

static String DeviceID="620134";
	
	static String pass="";
	static String baseurl="";
	static String uname="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	static String CreateAlarm="";
	static String GetAlarms="";
	static String SetOperation="";
	static String UpdatePosition="";
	static String Time="";
	static String SetAutoMode="";
	static String SendCellInfo="";
	static String SendMobile="";
	static String SendWiFi="";
	static String newpass="";
	
	static String CurrentTime="";
	
	
	static String CreateAlarm1="";
	static String unameR="";
	static String passR="";

	public ApiTest() {
		
		Time=CurrentDateTimeExample1.getDateTime();
		Properties prop = new Properties();
	    InputStream input = null;

	    try {

	        input = new FileInputStream("src/main/resources/configuarations.properties");

	        prop.load(input);

	        baseurl=prop.getProperty("url");
	        uname=prop.getProperty("username");
	        uname2=prop.getProperty("username2");
	        newpass=prop.getProperty("newpassword");
	        Dpath=prop.getProperty("DriverPathc");
	        ph=prop.getProperty("ph1");
	        CreateAlarm=prop.getProperty("createalarm");
	        GetAlarms=prop.getProperty("getalarms");
	        SetOperation=prop.getProperty("setoperation");
	        UpdatePosition=prop.getProperty("updateposition");
	        SetAutoMode=prop.getProperty("setautomode");
	        SendCellInfo=prop.getProperty("sendcellinfo");
	        SendMobile=prop.getProperty("sendmobile");
	        SendWiFi=prop.getProperty("sendwifi");
	        
	        unameR=prop.getProperty("RamUser1");
	        passR=prop.getProperty("RamPass1");

	    } catch (IOException ex) {
	        ex.printStackTrace();
	    } finally {
	        if (input != null) {
	            try {
	                input.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	}
	
	public void setAlarm(String Type) {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\r\n" + 
						"\"source\": {\r\n" + 
						"\"id\":\"620134\"},\r\n" + 
						"\"type\":\""+Type+"\",\r\n" + 
						"\"text\":\"New alarm for testing smartrules\",\r\n" + 
						"\"severity\":\"MINOR\",\r\n" + 
						"\"status\":\"ACTIVE\",\r\n" + 
						"\"time\":\""+Time+"\"\r\n" + 
						"}").when()
				.post(CreateAlarm).then().log().all()
				.statusCode(201).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void setAlarmTest4752() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\r\n" + 
						"\"source\":{\r\n" + 
						"\"id\":\"620134\"},\r\n" + 
						"\"type\":\"TestAlarm\",\r\n" + 
						"\"text\":\"I am an alarm\",\r\n" + 
						"\"severity\":\"MINOR\",\r\n" + 
						"\"status\":\"ACTIVE\",\r\n" + 
						"\"time\":\""+Time+"\"\r\n" + 
						"}").when()
				.post(CreateAlarm).then().log().all()
				.statusCode(201).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	
	public void getAlarms() {
		// Get Children
		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.auth()
				.basic(uname, newpass).when().get(GetAlarms).then().statusCode(200)
				.body("self", Matchers.equalTo("https://indiatransition.test-ram.m2m.telekom.com/alarm/alarms?pageSize=5&currentPage=1"))
				.body("alarms.severity", Matchers.hasItem("MAJOR")).extract();
		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void setOperation() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\r\n" + 
						"\"deviceId\":\"620134\",\r\n" + 
						"\"c8y_Restart\":{},\r\n" + 
						"\"description\":\"Restart device\"\r\n" + 
						"}").when()
				.post(SetOperation).then().log().all()
				.statusCode(201).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	

	public void setSeverityAlarm(String Type) {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\r\n" + 
						"\"source\": {\r\n" + 
						"\"id\":\"620134\"},\r\n" + 
						"\"type\":\""+Type+"\",\r\n" + 
						"\"text\":\"New alarm for testing severity smartrules\",\r\n" + 
						"\"severity\":\"MINOR\",\r\n" + 
						"\"status\":\"ACTIVE\",\r\n" + 
						"\"time\":\""+Time+"\"\r\n" + 
						"}").when()

				.post(CreateAlarm).then().log().all()
				.statusCode(201).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	public void updatePosition() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\r\n" + 
						"\"c8y_Position\":{\"alt\": 67,\r\n" + 
						"\"lng\": 6.95173,\r\n" + 
						"\"lat\":51.151977}\r\n" + 
						"}").when()
				.put(UpdatePosition).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void autoMode() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"category\":\"cellid\",\"key\":\"operation.mode\",\"value\":\"auto\"}").when()
				.post(SetAutoMode).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void manualMode() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"category\":\"cellid\",\"key\":\"operation.mode\",\"value\":\"manual\"}").when()
				.post(SetAutoMode).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	public void sendCellInfo1() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_CellInfo\":{\"cellTowers\":[{\"mobileCountryCode\":262,\"mobileNetworkCode\":1,\"locationAreaCode\":20995,\"cellId\":32444}]}}").when()
				.put(SendCellInfo).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void sendCellInfo2() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_CellInfo\":{\"cellTowers\":[{\"mobileCountryCode\":404,\"mobileNetworkCode\":71,\"locationAreaCode\":3311,\"cellId\":33372}]}}").when()
				.put(SendCellInfo).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void sendMobile1() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_Mobile\":{\"cellId\":\"11950\",\"mcc\":\"240\",\"mnc\":\"1\",\"lac\":\"3012\"}}").when()
				.put(SendMobile).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void sendMobile2() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_Mobile\":{\"cellId\":\"58210\",\"mcc\":\"262\",\"mnc\":\"1\",\"lac\":\"38146\"}}").when()
				.put(SendMobile).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	public void sendWiFi1() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_WlanSsidInformation\":[{\"macAddress\":\"fe:ec:da:1a:bc:38\",\"signalStrength\":-68},{\"macAddress\":\"fe:ec:da:1b:c0:75\",\"signalStrength\":-62},{\"macAddress\":\"60:33:4b:e4:38:f6\",\"signalStrength\":-89}]}").when()
				.put(SendWiFi).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
	
	public void sendWiFi2() {

		ExtractableResponse<Response> getChildrenResponse = given().log().all()
				.proxy("10.24.0.53",8080)
				.contentType("application+json").accept("application+json").auth()
				.basic(uname, newpass)
				.body("{\"c8y_WlanSsidInformation\":[{\"macAddress\":\"74:26:ac:6d:d6:cc\",\"signalStrength\":-89},{\"macAddress\":\"6c:9c:ed:eb:3a:30\",\"signalStrength\":-50}]}").when()
				.put(SendWiFi).then().log().all()
				.statusCode(200).extract();

		System.out.println("!!!!!!!!!!!!!!" + getChildrenResponse.asString() + "!!!!!!!!!!!!!!!!");

	}
}