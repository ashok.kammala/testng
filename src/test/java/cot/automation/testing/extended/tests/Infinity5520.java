package cot.automation.testing.extended.tests;


import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity5520 {
	
	static String pass = "";
	static String newpass="";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String emailWidget="";
	static String baseurl2="";
	static String emailPass="";
	
	@Test(priority=22)
	public void TestMethod5520() throws InterruptedException{
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("src/main/resources/configuarations.properties");
			prop.load(input);
			baseurl = prop.getProperty("url");
			uname = prop.getProperty("username");
			pass = prop.getProperty("password");
			newpass=prop.getProperty("newpassword");
			Dpath = prop.getProperty("DriverPathc");
			ph = prop.getProperty("ph1");
			emailWidget=prop.getProperty("emailwidget2");
			baseurl2=prop.getProperty("emailUrl");
			emailPass=prop.getProperty("thrashpwd");
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		driver.manage().window().maximize();

		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;

		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(uname);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(newpass);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Cockpit_Popup_Button"))));
		element.click();
		
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Groups_Dropdown"))));
		element.click();
		Thread.sleep(5000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Cot_Test_group"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_PlusIcon"))));
		element.click();
		Thread.sleep(5000);
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_PlusIcon_NewDashboard"))));
		element.click();
		element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_NewDashboard_Create_Table"))));
		element.clear();
		element.sendKeys("EmailWidget Dashboard");
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Create_Table_Save"))));
		element.click();
		Thread.sleep(1000);
		element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_NewDashboard"))));
	    element.click();
		element = wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_NewDashboard_AddWidget_Button"))));
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_AddWidget_Button"))));
		element.click();
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_Select_Widget"))));
		element.click();
		List<WebElement> myElements = driver
				.findElements(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Dropdown")));
		for (WebElement e : myElements) {
			if (e.getText().equalsIgnoreCase("Magenta Email")) {
				e.click();
				break;
			}
		}
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_Deviceadding"))));
		element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_WidgetType"))));
		element.sendKeys("emailwidget");
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_EventText"))));
		element.sendKeys("Email widget test for automation");
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Save_Button")));
		int y = element.getLocation().getY();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollTo(0," + y+ ")");
		Thread.sleep(2000);
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_EmailReceiver"))));
		element.sendKeys(emailWidget);
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_NewDashboard_Widget_Save_Button")));
		element.click();
		Thread.sleep(10000);
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_emailTrigger_Button")));
		element.click();
		driver.get(baseurl2);
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_login")));
		element.sendKeys("cotautomation");
		Select mails= new Select(driver.findElement(By.id("form-domain-id")));	
		mails.selectByVisibleText("opentrash.com");
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_pwd")));
		element.sendKeys(emailPass);
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_Submit")));
		element.click();
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_body")));
		element.click();
		Thread.sleep(2000);
		WebElement strvalue = driver.findElement(By.xpath("//div[@id='messageDialog']//div[@class='message-content']"));
		String actual=strvalue.getText();
		System.out.println(actual);
		if(actual.contains("Email widget test for automation")==true) {
			System.out.println("Mail comparision is success");
		}else {
			System.out.println("Mail comaprision failes");
		}
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_delete")));
		element.click();
		Thread.sleep(1000);
		element = driver.findElement(By.xpath(prop.getProperty("Xpath_EmailWidget_Thrashmail_DeleteMsg")));
		element.click();
	    driver.navigate().to("https://indiatransition.test-ram.m2m.telekom.com/apps/cockpit/index.html#/group/630641/dashboard");
	    Thread.sleep(2000);
	    element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Cot_Test_group"))));
		element.click();
		element = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_EmailWidget_NewDashboard"))));
		element.click();
		Thread.sleep(1000);
		element = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_More_Butotn"))));
		element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_RemoveDashboard_Button"))));
		element.click();
		element = wait.until(ExpectedConditions
				.elementToBeClickable(By.xpath(prop.getProperty("Xpath_NewDashboard_ModalWindow_OK_Button"))));
		element.click();		
		driver.navigate().refresh();
		Thread.sleep(2000);
		driver.close();		
		
	}
}
