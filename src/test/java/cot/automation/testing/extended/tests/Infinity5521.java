package cot.automation.testing.extended.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class Infinity5521 {

	static String pass = "";
	static String baseurl = "";
	static String uname = "";
	static String Dpath = "";
	static String ph = "";
	static String urlmail = "";
	static String uname2 = "";
	static String uname3 = "";
	static String uname4 = "";
	static String uname5 = "";
	static String newalias = "Akshay.M.Navada1";
	static String passName = "AkshayMNavada1";

	@SuppressWarnings("deprecation")
	@Test(priority=7)
	public void Test5521Method() throws InterruptedException, IOException {

		Properties prop = new Properties();
		InputStream input = null;
		String Dpath = null;
		String baseurl = null;
		String username6 = null;
		String pass3 = null;
	
		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");

			prop.load(input);
			baseurl = prop.getProperty("url");
			Dpath = prop.getProperty("DriverPathc");
			username6 = prop.getProperty("username6");
			pass3 = prop.getProperty("pass3");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		System.out.println(driver.getTitle());
		String myUrl = null;
		WebDriverWait wait = new WebDriverWait(driver, 2000);
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))).sendKeys(username6);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Password"))).sendKeys(pass3);
		driver.findElement(By.xpath(prop.getProperty("Xpath_Login"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Login"))));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Administration1"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Administration1"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(prop.getProperty("Xpath_Userbutton"))));
		driver.findElement(By.xpath(prop.getProperty("Xpath_Userbutton"))).click();
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//a[@href = '#/users/new']//i[@class = 'fa fw fa-user-plus']")));
		driver.findElement(By.xpath("//a[@href = '#/users/new']//i[@class = 'fa fw fa-user-plus']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class ='form-group col-sm-8']//input[@name = 'username']")));
		driver.findElement(By.xpath("//div[@class ='form-group col-sm-8']//input[@name = 'username']"))
				.sendKeys("cotlogintest2@trash-mail.com");
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class ='form-group col-sm-8']//input[@name = 'displayName']")));
		driver.findElement(By.xpath("//div[@class ='form-group col-sm-8']//input[@name = 'displayName']"))
				.sendKeys("cotlogintest2");
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@class ='form-group']//input[@name = 'email']")));
		driver.findElement(By.xpath("//div[@class ='form-group']//input[@name = 'email']"))
				.sendKeys("cotlogintest2@trash-mail.com");
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class ='col-sm-6 form-group']//input[@ng-model='user.firstName']")));
		driver.findElement(By.xpath("//div[@class ='col-sm-6 form-group']//input[@ng-model='user.firstName']"))
				.sendKeys("NewUser1");
		wait.until(ExpectedConditions.visibilityOfElementLocated(
				By.xpath("//div[@class ='col-sm-6 form-group']//input[@ng-model = 'user.lastName']")));
		driver.findElement(By.xpath("//div[@class ='col-sm-6 form-group']//input[@ng-model = 'user.lastName']"))
				.sendKeys("Test1");
		wait.until(ExpectedConditions.elementToBeClickable(
				By.xpath("//button[@ng-if='canEditUser(currentUser, user)'][contains(.,'Save')]")));
		driver.findElement(By.xpath("//button[@ng-if='canEditUser(currentUser, user)'][contains(.,'Save')]")).click();
		
		String baseUrl1 = "https://www.trash-mail.com/en/";
		driver.get(baseUrl1);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class ='actions']//a[@class= 'btn btn-large btn-success start-now']")));
		driver.findElement(By.xpath("//div[@class ='actions']//a[@class= 'btn btn-large btn-success start-now']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class = 'controls']//input[@id ='inputEmail']")));
		driver.findElement(By.xpath("//div[@class = 'controls']//input[@id ='inputEmail']")).sendKeys("cotlogintest2");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("fetch-mails")));
		driver.findElement(By.id("fetch-mails")).click();
		Thread.sleep(45000);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@id='refresh-inbox']")));
		driver.findElement(By.xpath("//a[@id='refresh-inbox']")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html[1]/body[1]/div[4]/section[1]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/div[1]/p[4]")));
		driver.findElement(By.xpath("/html[1]/body[1]/div[4]/section[1]/div[5]/table[1]/tbody[1]/tr[1]/td[1]/a[1]/div[1]/p[4]")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/div[4]")));
		String output = driver.findElement(By.xpath("/html[1]/body[1]/div[5]/div[1]/div[2]/div[4]")).getText();
		File DestFile = new File("C:\\Users\\A94832877\\Downloads\\jar.txt");
		FileUtils.writeStringToFile(DestFile, output);
		File file = new File("C:\\Users\\A94832877\\Downloads\\jar.txt");
		java.io.FileReader fread = new java.io.FileReader(file);
		BufferedReader bread = new BufferedReader(fread);
		String content = null;
		String url = null;
		while ((content = bread.readLine()) != null) {
			ArrayList<String> containedUrls = new ArrayList<String>();
			String urlRegex = "((?:http|https)(?::\\/{2}[\\w]+)(?:[\\/|\\.]?)(?:[^\\s\"]*))";
			Pattern pattern = Pattern.compile(urlRegex);
			java.util.regex.Matcher urlMatcher = pattern.matcher(content);
			while (urlMatcher.find()) {
				containedUrls.add(content.substring(urlMatcher.start(0), urlMatcher.end(0)));
				url = containedUrls.get(0);
				if (url.contains("https://indiatransition.test-ram.m2m.telekom.com?token="))
					myUrl = url;
				System.out.println("Extracted: " + myUrl);
			}
		}
		bread.close();
		if (myUrl != null) {
			driver.get(myUrl);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='New password (required)']")));
			driver.findElement(By.xpath("//input[@placeholder='New password (required)']")).sendKeys("@Ankitmafia818");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Confirm new password (required)']")));
			driver.findElement(By.xpath("//input[@placeholder='Confirm new password (required)']")).sendKeys("@Ankitmafia818");
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//form[@name='form_new_password']//button[@class='btn btn-primary btn-lg btn-block form-group'][contains(text(),'Set password')]")));
			driver.findElement(By.xpath("//form[@name='form_new_password']//button[@class='btn btn-primary btn-lg btn-block form-group'][contains(text(),'Set password')]")).click();
		}
	}

}
