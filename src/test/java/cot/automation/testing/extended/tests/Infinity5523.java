package cot.automation.testing.extended.tests;


import org.testng.annotations.Test;
import org.testng.AssertJUnit;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity5523  {
	static String pass="";
	static String newpass="";
	static String baseurl="";
	static String uname="";
	static String Dpath="";
	static String ph="";
	static String uname2="";
	
	
	@Test(priority=16)
	public void testMethod5523() throws InterruptedException, IOException {
		
		 Properties prop = new Properties();
		    InputStream input = null;

		    try {

		        input = new FileInputStream("src/main/resources/configuarations.properties");

		        prop.load(input);

		        baseurl=prop.getProperty("url");
		        uname=prop.getProperty("username");
		        uname2=prop.getProperty("username2");
		        pass=prop.getProperty("password");
		        newpass=prop.getProperty("newpassword");
		        Dpath=prop.getProperty("DriverPathc");
		        ph=prop.getProperty("ph1");
		        
		        
		    } catch (IOException ex) {
		        ex.printStackTrace();
		    } finally {
		        if (input != null) {
		            try {
		                input.close();
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
		    }
		    
		    
	System.setProperty("webdriver.chrome.driver", Dpath);
 	WebDriver driver = new ChromeDriver();
 	 driver.get(baseurl);
 	 driver.manage().window().maximize();
 	 
 	WebDriverWait wait=new WebDriverWait(driver, 120);
 	WebElement element;
 	
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
	element.sendKeys(uname);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
	element.sendKeys(newpass);
	element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
	element.click();
		System.out.println("COT Login is sucessfull");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		System.out.println("cockpit pop up button is visible");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Device_management"))));
		element.click();
		System.out.println("Device Management button is clicked");
		Thread.sleep(5000);
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Plus_Circle"))));
		element.click();
		System.out.println("clicked on add a new group");
		
		Thread.sleep(5000);
		Actions hover = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_New_Group")));
		hover.moveToElement(element).build().perform();
		Thread.sleep(3000);
		element.click();
		
		System.out.println("Add group button is displayed");
		System.out.println("create new group button is clicked");
		
		Thread.sleep(5000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_New_Group_Name1"))));
		element.click();
		element.sendKeys("AAAGroup");
		
		Thread.sleep(3000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Zero_Device_Group"))));
		element.click();
		
		//driver.navigate().refresh();
		Thread.sleep(2000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Group_Dropdown_New"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_AAAGroup2"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Sub_assets"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Assign_devices"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Enter_Device_Name"))));
		element.click();
		element.sendKeys("NewDevice1");
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Search_NewDevice1"))));
		element.click();
		Thread.sleep(3000);
		
		Actions hover2 = new Actions(driver);
		element= driver.findElement(By.xpath(prop.getProperty("Xpath_Checkbox_Of_The_Device")));
		hover2.moveToElement(element).build().perform();
		Thread.sleep(3000);
		element.click();
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Asign_One_Device"))));
		element.click();
		
		Thread.sleep(3000);
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Unassign"))));
		element.click();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Unassign_Device"))));
		element.click();
		Thread.sleep(3000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Unassign_The_Device"))));
		element.click();
		
	}

}
