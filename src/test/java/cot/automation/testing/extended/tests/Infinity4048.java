package cot.automation.testing.extended.tests;

import org.testng.annotations.Test;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Infinity4048 {
	static String Dpath = "";
	static String baseurl="";
	static String Runame="";
	static String Rpass="";
	
	@Test(priority=9)
	public static void Infinity4048one() throws InterruptedException, IOException {
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream("src/main/resources/configuarations.properties");
			prop.load(input);
			baseurl = prop.getProperty("url");
			Dpath = prop.getProperty("DriverPathc");
			Runame = prop.getProperty("Rusername");
			Rpass = prop.getProperty("Rpassword");

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
	
		System.setProperty("webdriver.chrome.driver", Dpath);
		WebDriver driver = new ChromeDriver();
		driver.get(baseurl);
		driver.manage().window().maximize();
		WebDriverWait wait = new WebDriverWait(driver, 120);
		WebElement element;
		
		System.out.println("login mask is visible");
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Username_Login_Page"))));
		element.sendKeys(Runame);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Password"))));
		element.sendKeys(Rpass);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Login"))));
		element.click();
		
		System.out.println("COT Login is sucessfull");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_c8y_icon_menu"))));
		element.click();
		
		System.out.println("Navigation to the Administration tab is successfull");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Administration"))));
		element.click();
		
		System.out.println("Click to the Administration tab is successfull");
		
		Thread.sleep(3000);
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Management"))));
		element.click();
		
		System.out.println("Click to management tab is successfull");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Files_Repository"))));
		element.click();
		
		System.out.println("Click on flie repository is successfull");
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Upload_File"))));
		element.click();
		
		System.out.println("Upload file button click is successfull");
		
		Thread.sleep(4000);
		driver.switchTo().activeElement();
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Select_File_To_Upload"))));
		element.click();
		Thread.sleep(5000);
		
		Runtime.getRuntime().exec("C:\\Users\\a94832877\\Desktop\\AutoIT_script\\AutoScript1.exe");
		Thread.sleep(10000);
		
		element= wait.until(ExpectedConditions.elementToBeClickable(By.xpath(prop.getProperty("Xpath_Upload_Button"))));
		element.click();
	}

}