package cot.automation.testing.extended.tests;

import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    
public class CurrentDateTimeExample1 {
	
	static int h=0;
	static int flag=0;
  public static String getDateTime() {    
	  
   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
   LocalDateTime now = LocalDateTime.now();  
   String datetime=dtf.format(now);
   String datetimenum=getDTNum(datetime);
   //System.out.println(datetimenum);
   
   String S1=getFormat(datetime);
   String S2=getFormat2(datetime);
   
   String mm=getMinute(datetimenum);
   String hh=getHour(datetimenum);
   String DT=S1+"T"+hh+":"+mm+":"+S2+"Z";
   //System.out.println(DT);
   
   return DT;
  }   
  
  public static String getDTNum(String s) {
		
	   String numberOnly= s.replaceAll("[^0-9]", "");
	   
	return numberOnly;
}
  public static String getMinute(String s) {
		
	   String minuteOnly= s.substring(10);
	   minuteOnly =minuteOnly.substring(0,minuteOnly.length()-2);
	   int i=Integer.parseInt(minuteOnly);
	   i=i+1+30;
	   if(i>60){
		   i=i-60;
		   flag=1;
	   }
	   String min=Integer.toString(i);
	return min;
}
  public static String getHour(String s) {
		
	   String hoursOnly= s.substring(8);
	   hoursOnly =hoursOnly.substring(0,hoursOnly.length()-4);
	   int h=Integer.parseInt(hoursOnly);
	   h=h-6;
	   if(flag==1) {
		   h=h+1;
		   if(h==24) {
			   h=0;
		   }
	   }
	   String hour=Integer.toString(h);
	return hour;
}
  
  public static String getFormat(String s) {
		String s1=s.substring(0,10);
		s1= s1.replaceAll("/", "-");
	
	return s1;
}
  
  public static String getFormat2(String s) {
		String s2=s.substring(s.length()-2);
	
	return s2;
}
  
  
  
}   